<?php

namespace App\Model\Search;

use App\Model\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class UserSearch
{
    /**
     * @param Request $request
     * @return Builder
     */
    public static function search(Request $request): Builder
    {
        $user = User::query();

        if ($request->has('name')) {
            $user->where('name', '=', $request->get('name'));
        }
        if ($request->has('email')) {
            $user->where('email', '=', $request->get('email'));
        }

        return $user;
    }


}