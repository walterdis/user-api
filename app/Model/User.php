<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    public $dates = ['birth', 'deleted_at'];

    /**
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'is_active', 'birth', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @author Walter Discher Cechinel
     *
     * @param String $date
     */
    public function setBirthAttribute($date)
    {

        $this->attributes['birth'] = Carbon::parse($date);
        return true;

        if (!$date instanceof \DateTime && !$date instanceof Carbon) {
            $date = Carbon::createFromFormat('d/m/Y', $date);
        }

        if ($date instanceof \DateTime) {
            /** @var $date \DateTime * */
            $date = Carbon::createFromTimestamp($date->getTimestamp());
        }

        $this->attributes['birth'] = $date->format('Y-m-d');
    }

    /**
     * @author Walter Discher Cechinel
     *
     * @param Carbon $birth
     * @return mixed
     */
    public function getBirthAttrifbute($birth)
    {
        var_dump($birth);
        exit;
        if (!$birth instanceof \DateTime && !$birth instanceof Carbon) {
            $birth = Carbon::createFromFormat('Y-m-d', $birth);
        }

        return $birth->format('d/m/Y');
    }

    /**
     * @author Walter Discher Cechnel
     *
     * @param string $password
     * @return string
     */
    public function setPasswordAttribute($password = null)
    {
        $this->attributes['password'] = Hash::make($password);
    }
}
