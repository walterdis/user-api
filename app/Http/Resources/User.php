<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class User extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'surname' => $this->surname,
            'full_name' => sprintf('%s %s', $this->name, $this->surname),
            'email' => $this->email,
            'birth' => $this->birth,
            'is_active' => $this->is_active,
        ];
    }
}
