<?php

namespace App\Http\Controllers;

use App\Http\Resources\User as UserResource;
use App\Model\Search\UserSearch;
use App\Model\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use function Psy\debug;

class UserController extends Controller
{
    /**
     * @author Walter Discher Cechinel
     *
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index(Request $request)
    {
        return UserResource::collection(UserSearch::search($request)->get());
    }

    /**
     * @author Walter Discher Cechinel
     *
     * @return User
     */
    public function create(Request $request)
    {
        return new UserResource(User::create($request->post()));

    }

    /**
     * @author Walter Discher Cechinel
     *
     * @param  int $id
     * @return User
     */
    public function show($id)
    {
        return new UserResource(User::find($id));

    }

    /**
     * @author Walter Discher Cechinel
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id)
    {
        if (!$user = $this->loadUser($id)) {
            return response()->json([
                'success' => false,
                'message' => 'Usuário não encontrado.'

            ])->setStatusCode(400);
        }

        if ($user->update($request->all())) {
            return [
                'success' => true,
                'data' => 'Usuário atualizado.'
            ];
        }

        return response()->json([
            'success' => false,
            'data' => 'Erro ao atualizar o usuário.'
        ])->setStatusCode(400);
    }

    /**
     * Hard delete de usuário
     *
     * @author Walter Discher Cechinel
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        return $this->removeUser($id, false);
    }

    /**
     * Soft delete de usuário
     * @author Walter Discher Cechinel
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return $this->removeUser($id);
    }

    /**
     * @author Walter Discher Cechinel
     *
     * @param $id
     * @param bool $softDelete
     * @return JsonResponse
     */
    private function removeUser(int $id, bool $softDelete = true)
    {
        if (!$user = $this->loadUser($id)) {
            return response()->json([
                'success' => false,
                'message' => 'Usuário não encontrado.'

            ])->setStatusCode(400);
        }

        $deleted = $softDelete ? $user->delete() : $user->forceDelete();


        if (!$deleted) {
            return response()->json([
                'success' => false,
                'data' => 'Erro ao excluir o usuário.'
            ])->setStatusCode(400);
        }

        return [
            'success' => true,
            'data' => 'Usuário excluído.'
        ];
    }

    /**
     * @author Walter Discher Cechinel
     * @param int $id
     * @return User|boolean
     */
    private function loadUser(int $id)
    {
        if (!$user = User::find($id)) {
            return false;
        }

        return $user;
    }
}
